package mappe.del2.GUI;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mappe.del2.factory.GuiWindow;
import mappe.del2.factory.SceneFactory;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {

        try{
            SceneFactory sf = new SceneFactory();

            GuiWindow window = sf.getScene("patientRegister");
            Scene scene = window.loadScene();


            stage.setScene(scene);
            stage.show();

        }catch (Exception e){
            throw new Exception(e);
        }
    }
}
