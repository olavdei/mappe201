package mappe.del2.GUI.controllers;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.stage.Stage;

/**
 * Controller for InformationDialog.fxml
 */
public class InformationDialog {


    public void closeWindow(ActionEvent actionEvent) {
        final Node source = (Node) actionEvent.getSource();
        final Stage stage = (Stage) source.getScene().getWindow();

        stage.close();
    }
}
