package mappe.del2.GUI.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;


import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import mappe.del2.factory.DeleteConfirmation;
import mappe.del2.factory.GuiWindow;
import mappe.del2.factory.NewPatientDialog;
import mappe.del2.factory.SceneFactory;
import mappe.del2.hospital.Pasient;
import mappe.del2.persistence.CSVManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for PatientRegister.fxml
 */
public class PatientRegister implements Initializable {
    private ObservableList<Pasient> people;
    CSVManager csvManager;

    @FXML private TableView<Pasient> tableView;
    @FXML private TableColumn<Pasient, String> firstNameColumn;
    @FXML private TableColumn<Pasient, String> lastNameColumn;
    @FXML private TableColumn<Pasient, String> ssnColumn;


    public void importFromCSV()  {
        FileChooser fc = new FileChooser();
        fc.setTitle("Load");
        if (csvManager != null){
            fc.setInitialDirectory(new File(csvManager.getFilePath()));
        }
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fc.getExtensionFilters().add(extFilter);

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        ssnColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        try {
            String path = fc.showOpenDialog(null).getPath();

            try {
                people = getPeopleCustom(path);
                tableView.setItems(people);
            } catch (Exception e) {
            }
            tableView.refresh();

        }catch (Exception fileChooserNull){

        }
    }

    public void exportToCSV() throws IOException {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(csvManager.getFilePath()));
        fc.setInitialFileName("newFile.csv");
        fc.setTitle("Save");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fc.getExtensionFilters().add(extFilter);
        try {
            String folderPath = fc.showSaveDialog(null).getAbsolutePath();

            if (!(folderPath == null || folderPath.equalsIgnoreCase(""))){
              try {
            csvManager.writeToNewFile(people, folderPath);
        }catch (Exception e){

             }
            }}catch (Exception fileChooserNull){

            }
    }

    public void quit() {
        Platform.exit();
    }

    public void addPatient(ActionEvent actionEvent) {
        Pasient newPatient = null;
        try {
            newPatient = new Pasient("", "", "", "12345678910");
        }catch (Exception e){
            e.printStackTrace();
        }

        SceneFactory factory = new SceneFactory();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        stage.setX(coordinateX() + 50);
        stage.setY(coordinateY());

        GuiWindow window = factory.getScene("newPatient");


        try {
            stage.setScene(window.loadScene());
            if (window instanceof NewPatientDialog){
                NewPatientDetails controller = ((NewPatientDialog) window).getController();
                controller.initData(newPatient, this);
            }
            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void editSelectedPatient(ActionEvent actionEvent) {
        Pasient patient = tableView.getSelectionModel().getSelectedItem();

        SceneFactory factory = new SceneFactory();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        stage.setX(coordinateX() + 50);
        stage.setY(coordinateY());

        GuiWindow window = factory.getScene("newPatient");


        try {
            stage.setScene(window.loadScene());
            if (window instanceof NewPatientDialog){
                NewPatientDetails controller = ((NewPatientDialog) window).getController();
                controller.initData(patient, this);
            }
            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void removeSelectedPatient(ActionEvent actionEvent) {
        Pasient patient = tableView.getSelectionModel().getSelectedItem();

        SceneFactory factory = new SceneFactory();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        stage.setX(coordinateX() + 50);
        stage.setY(coordinateY());

        GuiWindow window = factory.getScene("deleteConfirmation");

        try {
            stage.setScene(window.loadScene());
            if (window instanceof DeleteConfirmation){
                DeleteConfirmationController controller = ((DeleteConfirmation) window).getController();
                controller.initData(patient, this);
            }
            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void aboutWindow(ActionEvent actionEvent) {
        Stage stage = newWindow(actionEvent);
        assert stage != null;
        stage.setX(coordinateX() + 50);
        stage.setY(coordinateY());

        try {

            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Stage newWindow(ActionEvent event){
        SceneFactory factory = new SceneFactory();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        stage.setX(coordinateX() + 50);
        stage.setY(coordinateY());

        try {
            stage.setScene(factory.getScene("informationDialog").loadScene());
            return stage;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        ssnColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        try {
            people = FXCollections.observableArrayList();
            csvManager = new CSVManager();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void refresh(Pasient updatedPatient){
        System.out.println(people.size());
        if (!(people.contains(updatedPatient))){
            people.add(updatedPatient);
        }
        System.out.println(people.size());
        tableView.setItems(people);
        tableView.refresh();
    }

    public void removePatient(Pasient p){
        people.remove(p);
        tableView.setItems(people);
        tableView.refresh();
    }

    public ObservableList<Pasient> getPeopleCustom(String path) throws IOException {
        CSVManager csvManager = new CSVManager();
        return csvManager.readFileCustomPath(path);
    }

    public double coordinateX(){
        return this.tableView.getScene().getWindow().getX();
    }
    public double coordinateY(){
        return this.tableView.getScene().getWindow().getY();
    }
}
