package mappe.del2.GUI.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import mappe.del2.hospital.Pasient;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for DeleteConfirmation.fxml
 */
public class DeleteConfirmationController implements Initializable {
    private Pasient patient;
    private PatientRegister controller;

    @FXML private Label information;

    public void initData(Pasient patient, PatientRegister controller){
        this.patient = patient;
        this.controller = controller;

        this.information.setText("Patient: " + patient.getFirstName() + " " + patient.getLastName() + " " + patient.getSocialSecurityNumber());
    }

    public void deletePatient(ActionEvent actionEvent) {
        this.controller.removePatient(this.patient);

        final Node source = (Node) actionEvent.getSource();
        final Stage stage = (Stage) source.getScene().getWindow();

        stage.close();
    }

    public void closeWindow(ActionEvent actionEvent) {
        final Node source = (Node) actionEvent.getSource();
        final Stage stage = (Stage) source.getScene().getWindow();

        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
