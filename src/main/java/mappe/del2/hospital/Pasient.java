package mappe.del2.hospital;

import java.util.Objects;

/**
 * Represents Patient, class named in Norwegian because of task specification
 * Social security number must be 11 char long and not contain letters
 */
public class Pasient {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;
    private String diagnosis;
    private String generalPractitioner;

    public Pasient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber)throws Exception{
        if (socialSecurityNumber.length() != 11 || socialSecurityNumber.contains("[a-zA-Z]")) {
            throw new Exception("SSN error");

        }else {
            this.firstName = firstName;
            this.lastName = lastName;
            this.generalPractitioner = generalPractitioner;
            this.socialSecurityNumber = socialSecurityNumber;
            diagnosis = "";
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public String toString() {
        return "Pasient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pasient pasient = (Pasient) o;
        return socialSecurityNumber.equals(pasient.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
