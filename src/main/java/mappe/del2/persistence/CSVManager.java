package mappe.del2.persistence;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import mappe.del2.hospital.Pasient;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class CSVManager {
    String fileName;
    String filePath;
    ObservableList<Pasient> patientList;

    public CSVManager(){
        this.filePath = System.getProperty("user.home");
        this.fileName = "PatientTestData";

        this.patientList = FXCollections.observableArrayList();
    }
    public CSVManager(String fileName, String filePath){
        this.fileName = fileName;
        this.filePath = filePath;

        this.patientList = FXCollections.observableArrayList();
    }

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public ObservableList<Pasient> getPatientList() {
        return patientList;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setPatientList(ObservableList<Pasient> patientList) {
        this.patientList = patientList;
    }

    public ObservableList<Pasient> readFile() throws IOException {
        FileReader reader = new FileReader(this.filePath + "/" + this.fileName + ".csv");
        this.patientList = FXCollections.observableArrayList();

        CSVReader csvReader = new CSVReader(reader);
        try{
            String[] nextLine;

            while ((nextLine = csvReader.readNext()) != null){
                for (var e: nextLine){
                    System.out.println(e);
                    String[] patient = e.split(";");
                    try {
                        this.patientList.add(new Pasient(patient[0], patient[1], patient[2], patient[3]));

                    }catch (Exception e1){
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        reader.close();
        return patientList;
    }
    public ObservableList<Pasient> readFileCustomPath(String path) throws IOException {

        FileReader reader = new FileReader(path);
        this.patientList = FXCollections.observableArrayList();

        CSVReader csvReader = new CSVReader(reader);
        try{
            String[] nextLine;

            while ((nextLine = csvReader.readNext()) != null){
                for (var e: nextLine){
                    String[] patient = e.split(";");
                    try {
                        this.patientList.add(new Pasient(patient[0], patient[1], patient[2], patient[3]));

                    }catch (Exception e1){
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        reader.close();
        return patientList;
    }

    public Boolean writeToNewFile(ObservableList<Pasient> patientList, String filePath) throws IOException {
        List<String> patient;

        FileWriter csvWriter = new FileWriter(filePath);
        try {
            for (Pasient p: patientList) {
                patient = Arrays.asList(p.getFirstName(), p.getLastName(), p.getGeneralPractitioner(), p.getSocialSecurityNumber());
                csvWriter.append(String.join(";", patient));
                csvWriter.append("\n");
            }
            csvWriter.flush();
            csvWriter.close();

        }catch (Exception e){
            e.printStackTrace();
            csvWriter.close();
            return false;
        }
        csvWriter.close();
        return true;
    }
    
}
