package mappe.del2.factory;

import javafx.scene.Scene;

import java.io.IOException;

/**
 * Interface used to load correct FXML file
 */
public interface GuiWindow {
    Scene loadScene() throws IOException;

}
