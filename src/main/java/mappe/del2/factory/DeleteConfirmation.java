package mappe.del2.factory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import mappe.del2.GUI.controllers.DeleteConfirmationController;
import mappe.del2.GUI.controllers.NewPatientDetails;

import java.io.IOException;

/**
 * Class loads a delete confirmation window and can return the correct controller
 */
public class DeleteConfirmation implements GuiWindow{
    private FXMLLoader loader;

    @Override
    public Scene loadScene() throws IOException {
        this.loader = new FXMLLoader();
        this.loader.setLocation(getClass().getResource("/DeleteConfirmation.fxml"));


        AnchorPane root = loader.load();
        Scene scene = new Scene(root);

        return scene;
    }

    /**
     * Used to get correct controller
     * @return Spesific controller for instance of object
     * @throws IOException for unknown errors
     */
    public DeleteConfirmationController getController() throws IOException {
        if (this.loader != null) {
            return this.loader.getController();
        }
        loadScene();
        return this.loader.getController();
    }

}
