package mappe.del2.factory;


import mappe.del2.hospital.Pasient;

/**
 * Object is used to load the correct window for the main application
 */
public class SceneFactory {
    public GuiWindow getScene(String sceneType){
        if (sceneType == null){
            return null;
        }else if (sceneType.equalsIgnoreCase("patientRegister")){
            return new PatientRegister();
        }else if (sceneType.equalsIgnoreCase("deleteConfirmation")){
            return new DeleteConfirmation();
        }else if (sceneType.equalsIgnoreCase("informationDialog")){
            return new InformationDialog();
        }else if (sceneType.equalsIgnoreCase("newPatient")){
            return new NewPatientDialog();
        }
        return null;
    }
}