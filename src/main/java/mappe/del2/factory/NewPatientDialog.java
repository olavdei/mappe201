package mappe.del2.factory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import mappe.del2.GUI.controllers.NewPatientDetails;

import java.io.IOException;
/**
 * Class loads a new patient dialog, can be used for both editing and creating window and can return the correct controller
 */
public class NewPatientDialog implements GuiWindow{
    private FXMLLoader loader;
    @Override
    public Scene loadScene() throws IOException {
        this.loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/NewPatientDetails.fxml"));

        AnchorPane root = loader.load();
        Scene scene = new Scene(root);

        return scene;
    }

    /**
     * Used to get correct controller
     * @return Spesific controller for instance of object
     * @throws IOException for unknown errors
     */
    public NewPatientDetails getController() throws IOException {
        if (this.loader != null) {
            return this.loader.getController();
        }
        loadScene();
        return this.loader.getController();
    }
}
