package mappe.del2.persistence;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVManagerTest {
    @Test
    @DisplayName("CSVManager creation test")
    void CSVManagerCreationTest(){
        CSVManager c = new CSVManager();
        assertTrue(c.getFileName().equalsIgnoreCase("PatientTestData"));
        assertTrue(c.getFilePath().equals(System.getProperty("user.home")));

        c = new CSVManager("Test1", "Test2");
        assertEquals("Test1", c.getFileName());
        assertEquals("Test2", c.getFilePath());
    }
}