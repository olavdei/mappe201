package mappe.del2.factory;

import javafx.scene.Scene;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class SceneFactoryTest {

    @Test
    @DisplayName("Test factory creation")
    void FactoryCreationTest(){
        SceneFactory f = new SceneFactory();
        assertNotNull(f);
    }

    @Test
    @DisplayName("test scene factory PatientRegister is returned")
    void FactorReturnsPatientRegisterTest(){
        SceneFactory f = new SceneFactory();
        PatientRegister p = new PatientRegister();
        Object o = f.getScene("patientRegister");
        assertSame(p.getClass(), o.getClass());
    }

    @Test
    @DisplayName("test scene factory NewPatientDialog is returned")
    public void FactorReturnsNewPatientDetailsTest(){
        SceneFactory f = new SceneFactory();
        NewPatientDialog p = new NewPatientDialog();
        Object o = f.getScene("newPatient");
        assertSame(p.getClass(), o.getClass());
    }

    @Test
    @DisplayName("test scene factory InformationDialog is returned")
    void FactorReturnInformationDialogTest(){
        SceneFactory f = new SceneFactory();
        InformationDialog p = new InformationDialog();
        Object o = f.getScene("informationDialog");
        assertSame(p.getClass(), o.getClass());
    }

    @Test
    @DisplayName("test scene factory DeleteConfirmation is returned")
    void FactorReturnsDeleteConfirmationTest(){
        SceneFactory f = new SceneFactory();
        DeleteConfirmation p = new DeleteConfirmation();
        Object o = f.getScene("deleteConfirmation");
        assertSame(p.getClass(), o.getClass());
    }

    @Test
    @DisplayName("test scene factory Null is returned")
    void FactorReturnsNullTest(){
        SceneFactory f = new SceneFactory();
        assertNull(f.getScene("Something else"));
    }

}