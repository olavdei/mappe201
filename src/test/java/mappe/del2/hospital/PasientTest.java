package mappe.del2.hospital;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasientTest {

    @Test
    @DisplayName("Test pasient creation")
    void pasientCreationTests() throws Exception {
        Pasient p = new Pasient("Test", "Patient", "Doctor who", "12345678910");
        assertEquals("Test", p.getFirstName());
        assertEquals("Patient", p.getLastName());
        assertEquals("Doctor who", p.getGeneralPractitioner());
        assertEquals("12345678910", p.getSocialSecurityNumber());
        assertEquals("", p.getDiagnosis());
    }

    @Test
    @DisplayName("Exeption thrown if SSN shorter than 11 characters")
    void pasientCreationTestFailSsnToShort(){
        Exception e = assertThrows(Exception.class, () ->{
            new Pasient("Test", "Patient", "Doctor who", "1234567891");
        });

        String expected = "SSN error";
        String actual = e.getMessage();

        assertTrue(actual.contains(expected));
    }

    @Test
    @DisplayName("Exeption thrown if SSN longer than 11 characters")
    void pasientCreationTestFailSsnToLong(){
        Exception e = assertThrows(Exception.class, () ->{
            new Pasient("Test", "Patient", "Doctor who", "123456789101");
        });

        String expected = "SSN error";
        String actual = e.getMessage();

        assertTrue(actual.contains(expected));
    }

    @Test
    @DisplayName("Exeption thrown if SSN contains only letters")
    void pasientCreationTestFailSsnContainsOnlyLetters(){
        Exception e = assertThrows(Exception.class, () ->{
            new Pasient("Test", "Patient", "Doctor who", "abcdefghijk");
        });

        String expected = "SSN error";
        String actual = e.getMessage();

        assertTrue(actual.contains(expected));
    }

    @Test
    @DisplayName("Exeption thrown if SSN contains one letter")
    void pasientCreationTestFailSsnContainsOneLetter(){
        Exception e = assertThrows(Exception.class, () ->{
            new Pasient("Test", "Patient", "Doctor who", "1234567891a");
        });

        String expected = "SSN error";
        String actual = e.getMessage();

        assertTrue(actual.contains(expected));
    }

}